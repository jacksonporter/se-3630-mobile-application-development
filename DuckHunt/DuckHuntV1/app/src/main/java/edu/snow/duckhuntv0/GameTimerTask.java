package edu.snow.duckhuntv0;

import java.util.TimerTask;

/**
 * Created by jacks on 4/15/2018.
 */

public class GameTimerTask extends TimerTask {
    private Game game;
    private GameView gameView;

    public GameTimerTask(GameView view){
        gameView = view;
        game = gameView.getGame();
        game.startDuckFromRightTopHalf();
    }

    @Override
    public void run() {
        game.moveDuck();
        if(game.duckOffScreen()){
            game.startDuckFromRightTopHalf();
        }

        gameView.postInvalidate();
    }
}
