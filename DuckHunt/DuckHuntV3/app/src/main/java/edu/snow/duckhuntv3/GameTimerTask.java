package edu.snow.duckhuntv3;

import java.util.TimerTask;

/**
 * Created by jacks on 4/15/2018.
 */

public class GameTimerTask extends TimerTask {
    private Game game;
    private GameView gameView;

    public GameTimerTask(GameView view){
        gameView = view;
        game = gameView.getGame();
        game.startDuckFromRightTopHalf();
    }

    @Override
    public void run() {
        game.moveDuck();
        //Bullet
        if(game.bulletOffScreen()){
            game.loadBullet();
        }
        else if(game.isBulletFired()){
            game.moveBullet();
        }
        //Duck
        if(game.duckOffScreen()){
            game.setDuckShot(false);
            game.startDuckFromRightTopHalf();
        }
        else if(game.duckHit()){
            game.setDuckShot(true);
            ((MainActivity) gameView.getContext()).playHitSound();
            game.loadBullet();
        }
        //Update view
        gameView.postInvalidate();
    }
}
