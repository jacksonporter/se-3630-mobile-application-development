package edu.snow.duckhuntv3;

import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;

import java.util.Random;

/**
 * Created by jacks on 4/15/2018.
 */

public class Game {
    private Rect huntingRect;
    private int deltaTime;

    private Rect duckRect;
    private int duckWidth;
    private int duckHeight;
    private float duckSpeed;
    private boolean duckShot;

    private Point cannonCenter;
    private int cannonRadius;
    private int barrelLength;
    private int barrelRadius;
    private float cannonAngle;

    private Point bulletCenter;
    private int bulletRadius;
    private boolean bulletFired;
    private float bulletAngle;
    private float bulletSpeed;

    private Random random;

    public Game(Rect newDuckRect, int newBulletRadius, float newDuckSpeed, float newBulletSpeed){
        setDuckRect(newDuckRect);
        setDuckSpeed(newDuckSpeed);
        setBulletRadius(newBulletRadius);
        setBulletSpeed(newBulletSpeed);
        random = new Random();
        bulletFired = false;
        duckShot = false;
        cannonAngle = (float) Math.PI / 4;
    }


    public void startDuckFromRightTopHalf() {
        this.duckRect.left = huntingRect.right;
        this.duckRect.right = this.duckRect.left + this.duckWidth;
        this.duckRect.top = random.nextInt(huntingRect.bottom / 2);
        this.duckRect.bottom = duckRect.top + this.duckHeight;
    }

    public void moveDuck() {
        if(!this.duckShot){
            this.duckRect.left -= this.duckSpeed * this.deltaTime;
            this.duckRect.right -= this.duckSpeed * this.deltaTime;
        }
        else{
            this.duckRect.top += 5 * this.duckSpeed * this.deltaTime;
            this.duckRect.bottom += 5 * this.duckSpeed * this.deltaTime;
        }
    }

    public boolean duckOffScreen() {
        return this.duckRect.right < 0 || this.duckRect.bottom < 0 || this.duckRect.top > this.huntingRect.bottom || this.duckRect.left > this.huntingRect.right;
    }

    public void setDuckRect(Rect duckRect) {
        if(duckRect != null){
            duckWidth = duckRect.right - duckRect.left;
            duckHeight = duckRect.bottom - duckRect.top;
            this.duckRect = duckRect;
        }
    }

    public void setDuckSpeed(float duckSpeed) {
        if(duckSpeed > 0) {
            this.duckSpeed = duckSpeed;
        }
    }

    public void setBulletRadius(int bulletRadius) {
        if(bulletRadius > 0){
            this.bulletRadius = bulletRadius;
        }
    }

    public void setBulletSpeed(float bulletSpeed) {
        if(bulletSpeed > 0) {
            this.bulletSpeed = bulletSpeed;
        }
    }

    public Rect getHuntingRect() {
        return huntingRect;
    }

    public void setHuntingRect(Rect huntingRect) {
        if(huntingRect != null) {
            this.huntingRect = huntingRect;
        }
    }

    public void setDeltaTime(int deltaTime) {
        if(deltaTime > 0) {
            this.deltaTime = deltaTime;
        }
    }

    public Rect getDuckRect() {
        return duckRect;
    }

    public Point getCannonCenter() {
        return cannonCenter;
    }

    public int getCannonRadius() {
        return cannonRadius;
    }

    public int getBarrelLength() {
        return barrelLength;
    }

    public int getBarrelRadius() {
        return barrelRadius;
    }

    public Point getBulletCenter() {
        return bulletCenter;
    }

    public int getBulletRadius() {
        return bulletRadius;
    }

    public float getBulletSpeed() {
        return bulletSpeed;
    }

    public float getCannonAngle() {
        return cannonAngle;
    }

    public boolean isBulletFired() {
        return bulletFired;
    }

    public boolean isDuckShot() {
        return duckShot;
    }

    public void setCannon(Point cannonCenter, int cannonRadius, int barrelLength, int barrelRadius){
        if(cannonCenter != null && cannonRadius > 0 && barrelLength > 0){
            this.cannonCenter = cannonCenter;
            this.cannonRadius = cannonRadius;
            this.barrelLength = barrelLength;
            this.barrelRadius = barrelRadius;
            this.bulletCenter = new Point((int) (cannonCenter.x + cannonRadius * Math.cos(cannonAngle)), (int) (cannonCenter.y - cannonRadius * Math.sin(cannonAngle)));
        }
    }

    public void setCannonAngle(float cannonAngle){
        if(cannonAngle >= 0 && cannonAngle <= Math.PI / 2){
            this.cannonAngle = cannonAngle;
        }
        else if(cannonAngle < 0){
            this.cannonAngle = 0;
        }
        else{
            this.cannonAngle = (float) Math.PI / 2;
        }

        if(!this.isBulletFired()){
            loadBullet();
        }
    }

    public void fireBullet(){
        this.bulletFired = true;
        this.bulletAngle = this.cannonAngle;
    }

    public void setDuckShot(boolean duckShot){
        this.duckShot = duckShot;
    }

    public void moveBullet(){
        this.bulletCenter.x += this.bulletSpeed * Math.cos(this.bulletAngle) * this.deltaTime;
        this.bulletCenter.y -= this.bulletSpeed * Math.sin(this.bulletAngle) * this.deltaTime;
    }

    public boolean bulletOffScreen(){
        return this.bulletCenter.x  - this.bulletRadius > this.huntingRect.right || this.bulletCenter.y + this.bulletRadius < 0;
    }

    public void loadBullet() {
        this.bulletFired = false;
        this.bulletCenter.x = (int) (cannonCenter.x + this.cannonRadius * Math.cos(this.cannonAngle));
        this.bulletCenter.y= (int) (cannonCenter.y - this.cannonRadius * Math.sin(this.cannonAngle));
    }

    public boolean duckHit(){
        Log.w("Game", "Duck was hit");
        return this.duckRect.intersects(this.bulletCenter.x - this.bulletRadius,
                this.bulletCenter.y - this.bulletRadius, this.bulletCenter.x + this.bulletRadius,
                this.bulletCenter.y + this.bulletRadius);
    }
}
