package edu.snow.duckhuntv2;

import java.util.TimerTask;

/**
 * Created by jacks on 4/15/2018.
 */

public class GameTimerTask extends TimerTask {
    private Game game;
    private GameView gameView;

    public GameTimerTask(GameView view){
        gameView = view;
        game = gameView.getGame();
        game.startDuckFromRightTopHalf();
    }

    @Override
    public void run() {
        game.moveDuck();
        //Bullet
        if(game.bulletOffScreen()){
            game.loadBullet();
        }
        else if(game.isBulletFired()){
            game.moveBullet();
        }
        //Duck
        if(game.duckOffScreen()){
            game.startDuckFromRightTopHalf();
        }
        //Update view
        gameView.postInvalidate();
    }
}
