package edu.snow.duckhuntv2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;

/**
 * Created by jacks on 4/15/2018.
 */

public class GameView extends View {
    public static int DELTA_TIME = 100;
    private int[] TARGETS = {R.drawable.duck0, R.drawable.duck1, R.drawable.duck2, R.drawable.duck1};;
    private Paint paint;
    private Game game;
    private Bitmap[] ducks;
    private int duckFrame;

    public GameView(Context context, int width, int height) {
        super(context);
        ducks = new Bitmap[TARGETS.length];

        for(int i = 0; i < this.ducks.length; i++){
            this.ducks[i] = BitmapFactory.decodeResource(getResources(), this.TARGETS[i]);
        }

        float scale = ((float) width / (ducks[0].getWidth() * 5));
        Rect duckRect = new Rect(0, 0, width / 5, (int) (ducks[0].getHeight() * scale));

        game = new Game(duckRect, 5, 0.03f, 0.2f);
        game.setDuckSpeed(width * 0.00003f);
        game.setBulletSpeed(width * 0.0003f);
        game.setDeltaTime(GameView.DELTA_TIME);

        game.setHuntingRect(new Rect(0, 0, width, height));
        game.setCannon(new Point(0, height), width / 30, width / 15, width / 50);

        paint = new Paint();
        paint.setColor(0xFF000000);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(game.getBarrelRadius());
    }

    public void onDraw(Canvas canvas){
        //Log.w("GameView", "onDraw method hit, height is: " + height);
        super.onDraw(canvas);

        canvas.drawCircle(game.getCannonCenter().x, game.getCannonCenter().y, game.getCannonRadius(), paint);

        canvas.drawLine(game.getCannonCenter().x,
        game.getCannonCenter().y,
        game.getCannonCenter().x + game.getBarrelLength() * (float) (Math.cos(game.getCannonAngle())),
                game.getCannonCenter().y - game.getBarrelLength() * (float) (Math.sin(game.getCannonAngle())),
        paint);

        if(!game.bulletOffScreen()){
            canvas.drawCircle(game.getBulletCenter().x, game.getBulletCenter().y, game.getBulletRadius(), paint);
        }

        duckFrame = (duckFrame + 1) % ducks.length;

        canvas.drawBitmap(ducks[duckFrame], null, game.getDuckRect(), paint);
    }

    public Game getGame() {
        return game;
    }
}
