package edu.snow.tempconvertv0;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class ChangeModeActivity extends AppCompatActivity {
    public static final String CMA = "ChangeModeActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_change);
    }

    public void goBack(View view){
        Log.w(CMA, "Inside ChangeModeActivity:goBack\n");
        updateTempConvertorObject();
        this.finish();
        overridePendingTransition(R.anim.scalein, 0);
    }

    protected void onStart(){
        super.onStart();
        Log.w(CMA, "Inside ChangeModeActivity:onStart\n");
        updateView();
    }

    protected void onRestart() {
        super.onRestart();
        Log.w(CMA, "Inside ChangeModeActivity:onRestart\n");
    }

    protected void onResume() {
        super.onResume();
        Log.w(CMA, "Inside ChangeModeActivity:onResume\n");
    }

    protected void onPause() {
        super.onPause();
        Log.w(CMA, "Inside ChangeModeActivity:onPause\n");
    }

    protected void onStop() {
        super.onStop();
        Log.w(CMA, "Inside ChangeModeActivity:onsStop\n");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.w(CMA, "Inside ChangeModeActivity:onDestroy\n");
    }

    public void updateView(){
        TempConvertor tempConvertor = MainActivity.tempConvertor;

        if(tempConvertor.getMode() == TempConvertor.FROMFAHRENHEIT){
            RadioButton rbF = (RadioButton) findViewById(R.id.fahrenheightradiobutton);
            rbF.setChecked(true);
        }
        else if(tempConvertor.getMode() == TempConvertor.FROMCELCIUS){
            RadioButton rbC = (RadioButton) findViewById(R.id.celciusradiobutton);
            rbC.setChecked(true);
        }
    }

    public void updateTempConvertorObject(){
        Log.w(CMA, "Inside ChangeModeActivity:updateTempConvertorObject\n");
        TempConvertor tempConvertor = MainActivity.tempConvertor;

        //update the mode in model
        RadioButton rbF = (RadioButton) findViewById(R.id.fahrenheightradiobutton);
        RadioButton rbC = (RadioButton) findViewById(R.id.celciusradiobutton);

        if(rbF.isChecked()){
            if(tempConvertor.getMode() == TempConvertor.FROMCELCIUS)
            {
                tempConvertor.setDegrees(tempConvertor.getResult());
                tempConvertor.setMode(TempConvertor.FROMFAHRENHEIT);
            }
        }else if(rbC.isChecked()){
            if(tempConvertor.getMode() == TempConvertor.FROMFAHRENHEIT)
            {
                tempConvertor.setDegrees(tempConvertor.getResult());
                tempConvertor.setMode(TempConvertor.FROMCELCIUS);
            }
        }

        tempConvertor.setPreferences(this);
    }
}
