package edu.snow.tempconvertv0;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by jacks on 3/17/2018.
 */

public class TempConvertor {

    public static final int FROMFAHRENHEIT = 0;
    public static final int FROMCELCIUS = 1;
    private static final String PREFERENCE_MODE = "mode";
    private static final String PREFERENCE_DEGREES = "degrees";
    private float degrees;
    private int mode;

    public TempConvertor(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);

        setMode(pref.getInt(PREFERENCE_MODE, FROMFAHRENHEIT));
        setDegrees(pref.getFloat(PREFERENCE_DEGREES, 30.0f));

    }

    public void setPreferences(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();

        editor.putInt(PREFERENCE_MODE, this.mode);
        editor.putFloat(PREFERENCE_DEGREES, this.degrees);
        editor.commit();
    }


    public void setMode(int mode) {
        this.mode = mode;
    }

    public void setDegrees(float degrees) {
        this.degrees = degrees;
    }

    public int getMode(){
        return this.mode;
    }

    public float getDegrees(){
        return this.degrees;
    }

    public float getResult(){
        if(this.mode == this.FROMFAHRENHEIT){
            return ((this.degrees - 32) * 5 / 9);
        }
        else if(this.mode == this.FROMCELCIUS){
            return ((this.degrees  * 9) / 5) + 32;
        }

        return 0.0f;
    }
}
