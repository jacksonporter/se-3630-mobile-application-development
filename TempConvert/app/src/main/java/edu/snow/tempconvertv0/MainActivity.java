package edu.snow.tempconvertv0;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static final String MA = "MainActivity";
    public static TempConvertor tempConvertor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tempConvertor = new TempConvertor(this);

        setContentView(R.layout.activity_main);
    }

    public void convert(View view) {
        EditText input = (EditText) findViewById(R.id.userdegrees);
        tempConvertor.setDegrees(Float.parseFloat(input.getText().toString()));

        TextView result = (TextView) findViewById(R.id.result);
        result.setText("" + tempConvertor.getResult());
    }

    public void convert() {
        EditText input = (EditText) findViewById(R.id.userdegrees);
        tempConvertor.setDegrees(Float.parseFloat(input.getText().toString()));

        TextView result = (TextView) findViewById(R.id.result);
        result.setText("" + tempConvertor.getResult());
    }

    public void changeMode(View view){
        this.convert();

        Intent changeModeIntent = new Intent(getApplicationContext(), ChangeModeActivity.class);
        startActivity(changeModeIntent);
        overridePendingTransition(R.anim.rotatearound, 0);
    }

    protected void onStart(){
        super.onStart();
        Log.w(MA, "Inside MainActivity:onStart\n");

        updateView();
    }

    protected void onRestart() {
        super.onRestart();
        Log.w(MA, "Inside MainActivity:onRestart\n");
    }

    protected void onResume() {
        super.onResume();
        Log.w(MA, "Inside MainActivity:onResume\n");
    }

    protected void onPause() {
        super.onPause();
        Log.w(MA, "Inside MainActivity:onPause\n");
    }

    protected void onStop() {
        super.onStop();
        Log.w(MA, "Inside MainActivity:onsStop\n");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.w(MA, "Inside MainActivity:onDestroy\n");
    }

    public void updateView(){
        if (tempConvertor.getMode() == TempConvertor.FROMFAHRENHEIT){
            TextView inputTV = (TextView) findViewById(R.id.input_mode);
            TextView resultTV = (TextView) findViewById(R.id.result_mode);

            inputTV.setText(getString(R.string.label_fahrenheit));
            resultTV.setText(getString(R.string.label_celcius));

            EditText userinput = (EditText) findViewById(R.id.userdegrees);
            userinput.setText("" + tempConvertor.getDegrees());
        }
        else if(tempConvertor.getMode() == TempConvertor.FROMCELCIUS){
            TextView inputTV = (TextView) findViewById(R.id.input_mode);
            TextView resultTV = (TextView) findViewById(R.id.result_mode);

            inputTV.setText(getString(R.string.label_celcius));
            resultTV.setText(getString(R.string.label_fahrenheit));

            EditText userinput = (EditText) findViewById(R.id.userdegrees);
            userinput.setText("" + tempConvertor.getDegrees());
        }

        this.convert();
    }
}
