# SE 3630 - Mobile Application Development

![](https://www.snow.edu/pr/brand/images/signature.jpg)

##Mobile Application Development
#####Lots of code was provided in class by our professor, Kristal Ray (https://github.com/krayinut)

######Content from the course, including course work and other misc. work. Work in this repository is in the Java programming language with XML used for some UI elements.

This class taught how to make Android applications, using programmed UI elements, hard-coded UIs and various Android specfic programming practices.

Break down of applications/projects:

- CandyStore: This application keeps a database of candy items and will total them as you add them to your cart. You can add, edit and delete different candy items as well. It saves your data upon exit of the application. It contains 4 different activities for the operations mentioned above.

- DuckHunt: Shoot the duck by tapping the screen! Incoorperates moving UI elements, sounds, multi-touch events and more.

- MortgageCalculator: An application with persistent data storage, and two different activities. This is the most advanced app that we have built this semester. This app allows one to calculate basic home mortgages and their payments and saves the data each time you edit it to your local device.

- Puzzle: Detection of touches and swipes (gestures) are learned in this exercise. Simply move the tiles on the screen up and down until you have formed the sentence.

- TempConvert: This application converts between the Fahrenheit and Celsius temperature scales back and forth. It saves your data upon exit of the application. It contains two different activities, one allowing you to switch from which scale you would like to convert out of. Your data is automatically swapped from result to input when switching modes. It also contains animations between the two activities.

- TestApp: Creates a 'user' to be stored.

- TicTacToe: Learning by coding a basic TicTacToe GUI game for Android, we were able to learn how to create a GUI interface programmatically rather than a static interface in XML.

- TipCalculator: Providing basic functionality, this tip calculator allow you to enter a total for your bill as well as a tip percentage. It will give the total tip and total overall cost that you pay at the dinner table. All you need to do is type and it does the rest! Code provided by Kristal Ray of Snow College. This activity allowed me to further my knowledge on Event listeners as well as XML. 

- TrafficLight: By learning how to program the GUI programmatically in Java, we were able to create a stop light application that runs through the basics of a stop light. 
