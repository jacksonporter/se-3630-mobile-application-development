package edu.snow.candystorev1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

/**
 * Created by jackson.porter on 3/25/2018.
 */

public class InsertActivity extends AppCompatActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
    }

    public void insert(View view){
        EditText nameET = findViewById(R.id.input_name);
        EditText priceET = findViewById(R.id.input_price);

        String name = nameET.getText().toString();
        float price = Float.parseFloat(priceET.getText().toString());

        /*
        Insert the information into the database here.
         */

        nameET.setText("");
        priceET.setText("");

    }

    public void goBack(View view){
        this.finish();
    }
}
