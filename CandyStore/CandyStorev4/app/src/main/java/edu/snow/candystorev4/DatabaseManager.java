package edu.snow.candystorev4;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by jackson.porter on 3/25/2018.
 */

public class DatabaseManager extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "candyDB";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_CANDY = "candy";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String PRICE = "price";

    public DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlCreate = "create table " + this.TABLE_CANDY + "( " + this.ID;
        sqlCreate += " integer primary key autoincrement, " + this.NAME;
        sqlCreate += " text, " + this.PRICE;
        sqlCreate += " real )";

        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + this.TABLE_CANDY);

        onCreate(db);
    }

    public void insert(Candy candy){
        SQLiteDatabase db = this.getWritableDatabase();
        String sqlInsert = "insert into " + this.TABLE_CANDY;
        sqlInsert += " values(null, '" + candy.getName();
        sqlInsert += "', '" + candy.getPrice() + "' )";
        db.execSQL(sqlInsert);
        db.close();
    }

    public void updateByID(int id, String name, double price){
        SQLiteDatabase db = this.getWritableDatabase();
        String sqlUpdate = "update " + this.TABLE_CANDY;
        sqlUpdate += " set " + this.NAME + " = '" + name + "', ";
        sqlUpdate += this.PRICE + " = '" + price + "'";
        sqlUpdate += " where " + this.ID + " = " + id;

        db.execSQL(sqlUpdate);
        db.close();
    }

    public void deleteByID(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String sqlDelete = "delete from " + this.TABLE_CANDY;
        sqlDelete += " where " + this.ID + " = " + id;

        db.execSQL(sqlDelete);
        db.close();
    }

    public Candy selectByID(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String sqlQuery = "select * from " + this.TABLE_CANDY;
        sqlQuery += " where " + this.ID + " = " + id;

        Cursor cursor = db.rawQuery(sqlQuery, null);

        Candy candy = null;

        if(cursor.moveToFirst()){
            candy = new Candy(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getDouble(2));
        }
        db.close();

        return candy;
    }

    public ArrayList<Candy> selectAll(){
        ArrayList<Candy> candies = new ArrayList<Candy>();
        SQLiteDatabase db = this.getWritableDatabase();

        String sqlQuery = "select * from " + this.TABLE_CANDY;
        Cursor cursor = db.rawQuery(sqlQuery, null);

        while(cursor.moveToNext()){
            Candy currentCandy = new Candy(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getDouble(2));
            candies.add(currentCandy);
        }
        db.close();

        return candies;
    }
}
