package edu.snow.candystorev4;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by jackson.porter on 3/25/2018.
 */

public class UpdateActivity extends AppCompatActivity {
    private DatabaseManager dbManager;
    private static final int NUMCOLUMNS = 4;
    private static final int NAMETOPRICE = 2;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbManager = new DatabaseManager(this);

        updateView();
    }

    public void updateView(){
        ArrayList<Candy> candies = dbManager.selectAll();

        if(candies.size() > 0){
            ScrollView scrollView = new ScrollView(this);
            GridLayout layout = new GridLayout(this);

            layout.setRowCount(candies.size());
            layout.setColumnCount(this.NUMCOLUMNS);

            TextView [] ids = new TextView[candies.size()];
            EditText [][] namesAndPrices = new EditText[candies.size()][this.NAMETOPRICE];
            Button [] buttons = new Button[candies.size()];
            ButtonHandler bh = new ButtonHandler();

            Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            int width = size.x;

            int i = 0;

            for(Candy candy : candies){
                ids[i] = new TextView(this);
                ids[i].setGravity(Gravity.CENTER);
                ids[i].setText("" + candy.getId());

                namesAndPrices[i][0] = new EditText(this);
                namesAndPrices[i][1] = new EditText(this);

                namesAndPrices[i][0].setText(candy.getName());
                namesAndPrices[i][1].setText("" + candy.getPrice());
                namesAndPrices[i][1].setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);

                namesAndPrices[i][0].setId(10 * candy.getId());
                namesAndPrices[i][1].setId(10 * candy.getId() + 1);

                buttons[i] = new Button(this);
                buttons[i].setText("Update");
                buttons[i].setId(candy.getId());
                buttons[i].setOnClickListener(bh);

                layout.addView(ids[i], width / 10, ViewGroup.LayoutParams.WRAP_CONTENT);
                layout.addView(namesAndPrices[i][0], (int) (width * .4), ViewGroup.LayoutParams.WRAP_CONTENT);
                layout.addView(namesAndPrices[i][1], (int) (width * .15), ViewGroup.LayoutParams.WRAP_CONTENT);
                layout.addView(buttons[i], (int) (width * .35), ViewGroup.LayoutParams.WRAP_CONTENT);

                i++;
            }

            scrollView.addView(layout);
            setContentView(scrollView);
        }
    }

    private class ButtonHandler implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            int candyID = view.getId();
            EditText nameET = (EditText) findViewById(10 * candyID);
            EditText priceET = (EditText) findViewById(10 * candyID + 1);
            String name = nameET.getText().toString();
            String priceString = priceET.getText().toString();

            try{
                double price = Double.parseDouble(priceString);
                dbManager.updateByID(candyID, name, price);
                Toast.makeText(UpdateActivity.this, "Candy updated", Toast.LENGTH_SHORT).show();

                updateView();
            }catch(NumberFormatException nfe)
            {
                Toast.makeText(UpdateActivity.this, "Price is formatted incorrectly", Toast.LENGTH_LONG).show();
            }
        }
    }
}
