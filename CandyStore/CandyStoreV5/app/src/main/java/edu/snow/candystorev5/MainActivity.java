package edu.snow.candystorev5;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private DatabaseManager dbManager;
    private double total;
    private int buttonWidth;
    private ScrollView scrollView;
    private int NUMCOLUMNS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        dbManager = new DatabaseManager(this);
        total = 0.0;
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        buttonWidth = size.x / 2;

        updateView();
    }

    @Override
    protected void onResume(){
        super.onResume();
        updateView();
    }

    public void updateView(){
        ArrayList<Candy> candies = dbManager.selectAll();
        GridLayout grid = new GridLayout(this);

        if(candies.size() > 0) {
            scrollView.removeAllViewsInLayout();
            grid.setRowCount((candies.size() + 1) / 2);
            grid.setColumnCount(this.NUMCOLUMNS);

            CandyButton[] buttons = new CandyButton[candies.size()];
            ButtonHandler bh = new ButtonHandler();

            int i = 0;

            for (CandyButton button : buttons) {
                buttons[i] = new CandyButton(this, candies.get(i));
                buttons[i].setText(candies.get(i).getName() + "\n" + candies.get(i).getPrice());
                buttons[i].setOnClickListener(bh);

                grid.addView(buttons[i], this.buttonWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
                i++;
            }

            scrollView.addView(grid);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id){
            case R.id.action_reset:
                Log.w("MainActivity", "Reset selected");
                this.total = 0.0;
                Toast.makeText(MainActivity.this, "Total Reset to $0.00", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_add:
                Log.w("MainActivity", "Add selected");
                Intent insertIntent = new Intent(this, InsertActivity.class);
                this.startActivity(insertIntent);
                return true;
            case R.id.action_delete:
                Log.w("MainActivity", "Delete selected");
                Intent deleteIntent = new Intent(this, DeleteActivity.class);
                this.startActivity(deleteIntent);
                return true;
            case R.id.action_update:
                Log.w("MainActivity", "Update selected");
                Intent updateIntent = new Intent(this, UpdateActivity.class);
                this.startActivity(updateIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class ButtonHandler implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            CandyButton thisButton = (CandyButton) view;
            total += thisButton.getPrice();
            String pay = NumberFormat.getCurrencyInstance().format(total);

            Toast.makeText(MainActivity.this, pay, Toast.LENGTH_SHORT).show();
        }
    }
}
