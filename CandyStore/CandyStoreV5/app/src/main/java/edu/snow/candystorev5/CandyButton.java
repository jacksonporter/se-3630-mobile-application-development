package edu.snow.candystorev5;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Button;

/**
 * Created by jackson.porter on 3/28/2018.
 */

@SuppressLint("AppCompatCustomView")
public class CandyButton extends Button {
    private Candy candy;

    public CandyButton(Context context, Candy candy){
        super(context);
        this.candy = candy;
    }

    public double getPrice(){
        return candy.getPrice();
    }
}
