package edu.snow.candystorev3;

/**
 * Created by jackson.porter on 3/25/2018.
 */

public class Candy {
    private int id;
    private String name;
    private double price;
    
    public Candy(int id, String name, double price){
        setID(id);
        setName(name);
        setPrice(price);
    }

    public void setID(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        if(price > 0.0) {
            this.price = price;
        }
    }

    public int getID() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public double getPrice() {
        return this.price;
    }

    @Override
    public String toString(){
        return this.id + "; " + this.name + "; " + this.price;
    }
}
