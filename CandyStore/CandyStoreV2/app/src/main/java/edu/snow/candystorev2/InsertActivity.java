package edu.snow.candystorev2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by jackson.porter on 3/25/2018.
 */

public class InsertActivity extends AppCompatActivity {
    private DatabaseManager dbManager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbManager = new DatabaseManager(this);

        setContentView(R.layout.activity_insert);
    }

    public void insert(View view){
        EditText nameET = findViewById(R.id.input_name);
        EditText priceET = findViewById(R.id.input_price);

        String name = nameET.getText().toString();
        String priceString = priceET.getText().toString();

        try{
            double price = Double.parseDouble(priceString);
            Candy candy = new Candy(0, name, price);
            dbManager.insert(candy);

            Toast.makeText(this, "Candy added", Toast.LENGTH_SHORT).show();
        }catch(NumberFormatException nfe){
            Toast.makeText(this, "Price is formatted incorrectly", Toast.LENGTH_LONG).show();
        }

        nameET.setText("");
        priceET.setText("");

    }

    public void goBack(View view){
        this.finish();
    }
}
