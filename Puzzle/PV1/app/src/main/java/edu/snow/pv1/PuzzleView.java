package edu.snow.pv1;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

class PuzzleView extends RelativeLayout {
    private TextView[] tvs;
    private LayoutParams[] params;
    private int[] colors;

    private int labelHeight;
    private int startY;
    private int startTouchY;
    private int emptyPosition;
    private int[] positions;

    public PuzzleView(Activity activity, int width, int height, int numberOfPeices) {
        super(activity);
        
        buildGUIByCode(activity, width, height, numberOfPeices);
    }

    private void buildGUIByCode(Activity activity, int width, int height, int numberOfPeices) {
        positions = new int[numberOfPeices];
        tvs = new TextView[numberOfPeices];
        colors = new int[tvs.length];
        params = new LayoutParams[tvs.length];
        Random random = new Random();
        labelHeight = height / numberOfPeices;

        for(int i = 0; i < tvs.length; i++){
            tvs[i] = new TextView(activity);
            tvs[i].setGravity(Gravity.CENTER);
            colors[i] = Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255));
            tvs[i].setBackgroundColor(colors[i]);
            params[i] = new LayoutParams(width, labelHeight);
            params[i].leftMargin = 0;
            params[i].topMargin = labelHeight * i;
            addView(tvs[i], params[i]);
        }
    }

    public void fillGUI(String[] scrambled) {
        for(int i = 0; i < tvs.length; i++){
            tvs[i].setText(scrambled[i]);
            positions[i] = i;
        }
    }

    public int indexOfTextView(View v) {
        if(!(v instanceof TextView)){
            return -1;
        }
        for(int i = 0; i < tvs.length; i++){
            if(v == tvs[i]){
                return i;
            }
        }
        return -1;
    }

    public void updateStartPositions(int index, int y) {
        startY = params[index].topMargin;
        startTouchY = y;
        emptyPosition = tvPosition(index);
    }

    public void moveTextViewVertically(int index, int y) {
        params[index].topMargin = startTouchY + y - startTouchY;
        tvs[index].setLayoutParams(params[index]);
    }

    public int tvPosition(int index) {
        return (params[index].topMargin + labelHeight/2) / labelHeight;
    }

    public void placeTextViewAtPosition(int index, int newPosition) {
        params[index].topMargin = newPosition * labelHeight;
        tvs[index].setLayoutParams(params[index]);
        int i = positions[newPosition];
        params[i].topMargin = emptyPosition * labelHeight;
        tvs[index].setLayoutParams(params[i]);
        positions[emptyPosition] = i;
        positions[newPosition] = index;
    }

    public String[] currentSolution() {
        String[] current = new String[tvs.length];
        for(int i = 0; i < current.length; i++){
            current[i] = tvs[positions[i]].getText().toString();
        }

        return current;
    }

    public void enableListener(OnTouchListener listener){
        for(int i = 0; i < tvs.length; i++){
            tvs[i].setOnTouchListener(listener);
        }
    }

    public void disableListener() {
        for(int i = 0; i < tvs.length; i++){
            tvs[i].setOnTouchListener(null);
        }
    }
}
