package edu.snow.puzzlev0;

import java.util.Random;

/**
 * Created by jackson.porter on 4/2/2018.
 */

public class Puzzle {
    public static final int NUMBER_PARTS = 5;
    String [] parts;
    Random random;

    public Puzzle(){
        parts = new String[NUMBER_PARTS];
        parts[0] = "I LOVE";
        parts[1] = "MOBILE";
        parts[2] = "PROGRAMMING";
        parts[3] = "USING";
        parts[4] = "JAVA";

        random = new Random();
    }

    public boolean solved(String [] solution){
        if(solution != null && solution.length == parts.length){
            for(int i = 0; i < parts.length; i++){
                if(!solution[i].equals(parts[i])){
                    return false;
                }
            }

            return true;
        }
        else{
            return false;
        }
    }

    public String[] scramble() {
        String [] scrambled = new String[parts.length];
        for(int i = 0; i < scrambled.length; i++){
            scrambled[i] = parts[i];
        }

        while(solved(scrambled)){
            for(int k = 0; k < scrambled.length; k++){
                int n = random.nextInt(scrambled.length - k) + 1;
                String temp = scrambled[k];
                scrambled[k] = scrambled[n];
                scrambled[n] = temp;
            }
        }

        return scrambled;
    }

    public int getNumberOfParts() {
        return parts.length;
    }
}
