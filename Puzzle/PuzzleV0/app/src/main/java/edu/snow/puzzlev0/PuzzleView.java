package edu.snow.puzzlev0;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by jackson.porter on 4/2/2018.
 */

public class PuzzleView extends RelativeLayout{
    TextView[] textViews;
    RelativeLayout.LayoutParams[] layoutParams;
    int[] colors;
    int labelHeight;

    public PuzzleView(Activity activity, int width, int height, int pieces){
        super(activity);

        buildGUIByCode(activity, width, height, pieces);
    }

    public void buildGUIByCode(Activity activity, int width, int height, int pieces){
        textViews = new TextView[pieces];
        colors = new int[textViews.length];
        layoutParams = new RelativeLayout.LayoutParams[textViews.length];

        labelHeight = height/pieces;

        //Generate random colors
        Random random = new Random();

        for(int i = 0; i < colors.length; i++) {
            textViews[i] = new TextView(activity);
            textViews[i].setGravity(Gravity.CENTER);

            colors[i] = Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255));

            textViews[i].setBackgroundColor(colors[i]);

            layoutParams[i] = new RelativeLayout.LayoutParams(width, labelHeight);
            layoutParams[i].leftMargin = 0;
            layoutParams[i].topMargin = labelHeight * i;

            addView(textViews[i], layoutParams[i]);
        }
    }

    public void fillGUI(String[] scrambledText){
        for(int i = 0; i < textViews.length; i++){
            textViews[i].setText(scrambledText[i]);
        }
    }
}
