package edu.snow.pv0;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

class PuzzleView extends RelativeLayout {
    private TextView[] tvs;
    private LayoutParams[] params;
    private int[] colors;

    private int labelHeight;

    public PuzzleView(Activity activity, int width, int height, int numberOfPeices) {
        super(activity);
        
        buildGUIByCode(activity, width, height, numberOfPeices);
    }

    private void buildGUIByCode(Activity activity, int width, int height, int numberOfPeices) {
        tvs = new TextView[numberOfPeices];
        colors = new int[tvs.length];
        params = new LayoutParams[tvs.length];
        Random random = new Random();
        labelHeight = height / numberOfPeices;

        for(int i = 0; i < tvs.length; i++){
            tvs[i] = new TextView(activity);
            tvs[i].setGravity(Gravity.CENTER);
            colors[i] = Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255));
            tvs[i].setBackgroundColor(colors[i]);
            params[i] = new LayoutParams(width, labelHeight);
            params[i].leftMargin = 0;
            params[i].topMargin = labelHeight * i;
            addView(tvs[i], params[i]);
        }
    }

    public void fillGUI(String[] scrambled) {
        for(int i = 0; i < tvs.length; i++){
            tvs[i].setText(scrambled[i]);
        }
    }
}
