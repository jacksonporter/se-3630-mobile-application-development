package com.example.jacksonporter.tictactoev0;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.GridLayout;

public class MainActivity extends AppCompatActivity {
    //Global variables
    private Button[][] buttons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main)

        //We are creating are GUI by coding it in dynamically.
        buildGUIbycode();
    }

    public void buildGUIbycode(){
        //Get width of the screen
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        //Calcuate the width of the buttons
        int w = size.x / TicTacToe.SIDE;

        //Create GridLayout as the layout manager
        GridLayout gridLayout = new GridLayout(this);
        gridLayout.setColumnCount(TicTacToe.SIDE);
        gridLayout.setRowCount(TicTacToe.SIDE);

        //Create buttons and add them to the GridLayout
        buttons = new Button[TicTacToe.SIDE][TicTacToe.SIDE];

        for(int i = 0; i < TicTacToe.SIDE; i++){
            for(int k = 0; k < TicTacToe.SIDE; k++){
                buttons[i][k] = new Button(this);
                gridLayout.addView(buttons[i][k], w, w);
            }
        }

        setContentView(gridLayout);
    }
}
