package com.example.jacksonporter.tictactoev0;

/**
 * Created by jacksonporter on 2/10/18.
 */

public class TicTacToe {
    //Global Variables
    public static final int SIDE = 3;
    private int[][] game;
    private int turn;

    /*
    Creates a new TicTacToe object.
     */
    public TicTacToe(){
       game = new int[SIDE][SIDE];
       resetGame();
    }

    /*
    Attempts to play on a certain location based on the current turn. If play is sucessful, successful turn is returned.
     */
    public int play(int row, int column){
        //Check to see if play could be made, return 0 if play was sucessful.
        int currentTurn = turn;

        if(game[row][column] == 0 && row >= 0 && column >= 0 && row < this.SIDE && column < this.SIDE){

            game[row][column] = turn;

            if(turn == 1){
               turn = 2;
            }
            else{
                turn = 1;
            }

            //Return 0, as it was the last value at this location
            return currentTurn;
        }
        //Return 0 if play could not be made here (unsuccessful play)
        else{
            return 0;
        }
    }

    public int whoWon(){
        int rows = checkRows();
        if(rows > 0)
            return rows;
        int columns = checkColumns();
        if(columns > 0)
            return columns;
        int diagonals = checkDiagonals();
        if(diagonals > 0)
            return diagonals;

        return 0;
    }


    //Winning play checks

    /*
    See if we have a winner in any of the rows.
     */
    public int checkRows() {
        for (int i = 0; i < this.SIDE; i++) {
            if (game[i][0] != 0 &&
                    game[i][0] == game[i][1] &&
                    game[i][1] == game[i][2]) {
                return game[i][0];
            }
        }
        return 0;
    }

    /*
   See if we have a winner in any of the columns.
    */
    public int checkColumns(){
        for(int i = 0; i < this.SIDE; i++){
            if(game[0][i] != 0 &&
                    game[0][i] == game[1][i] &&
                    game[1][i] == game[2][i]){
                return game[0][i];
            }
        }
        return 0;
    }

    /*
   See if we have a winner in any of the diagonals.
    */
    public int checkDiagonals(){
        if(game[0][0] != 0 &&
                game[0][0] == game[1][1] &&
                game[1][1] == game[2][2]){
            return game[0][0];
        }
        if(game[0][2] != 0 &&
                game[0][2] == game[1][1] &&
                game[1][1] == game[2][0]) {
            return game[0][2];
        }
        else {
            return 0;
        }
    }

    /*
    Checks to see if there is at least one empty spot on the board.
     */
    public boolean canStillPlay(){
        boolean emptySpot = false;

        for(int i = 0; i < 3 && !emptySpot; i++){
            for(int k = 0; k < 3 && !emptySpot; k++){
                if(game[i][k] == 0)
                {
                    emptySpot = true;
                }
            }
        }

        return emptySpot;
    }



    public boolean gameOver(){
        return !canStillPlay() || (whoWon() > 0);
    }

    /*
    Resets game
     */
    public void resetGame(){
        //Sets turn to 1
        turn = 1;

        //Clears (and sets) board to empty
        for(int i = 0; i < 3; i++){
            for(int k = 0; k < 3; k++){
                game[i][k] = 0;
            }
        }
    }
}
