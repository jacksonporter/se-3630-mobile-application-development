package com.example.jacksonporter.tictactoev3;

import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Global variables
    private Button[][] buttons;
    private TicTacToe tttGame;
    private TextView status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main)

        tttGame = new TicTacToe();

        //We are creating are GUI by coding it in dynamically.
        buildGUIbycode();
    }

    public void buildGUIbycode(){
        //Get width of the screen
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        //Calcuate the width of the buttons
        int w = size.x / TicTacToe.SIDE;

        //Create GridLayout as the layout manager
        GridLayout gridLayout = new GridLayout(this);
        gridLayout.setColumnCount(TicTacToe.SIDE);
        gridLayout.setRowCount(TicTacToe.SIDE + 1);

        //Create buttons and add them to the GridLayout
        //Also add a listener to complete an action for the buttons (display 'X')
        buttons = new Button[TicTacToe.SIDE][TicTacToe.SIDE];
        ButtonHandler bh = new ButtonHandler();

        for(int i = 0; i < TicTacToe.SIDE; i++){
            for(int k = 0; k < TicTacToe.SIDE; k++){
                buttons[i][k] = new Button(this);
                buttons[i][k].setOnClickListener(bh);
                buttons[i][k].setTextSize((int) (w * .18));
                gridLayout.addView(buttons[i][k], w, w);
            }
        }


        //Add status text view
        GridLayout.Spec rowSpec = GridLayout.spec(3, 1);
        GridLayout.Spec columnSpec = GridLayout.spec(0, 3);
        GridLayout.LayoutParams lp = new GridLayout.LayoutParams(rowSpec, columnSpec);

        status = new TextView(this);
        status.setLayoutParams(lp);
        //Set properties of Status Text View
        status.setBackgroundColor(Color.GREEN);
        status.setWidth(TicTacToe.SIDE * w);
        status.setHeight(w);
        status.setGravity(Gravity.CENTER);

        status.setText("Play!");
        status.setTextSize((int) (w * .1));


        //Add status textview to the layout
        gridLayout.addView(status);

        setContentView(gridLayout);
    }

    public void update(int row, int col){
        Log.w("MainActivity", "Inside update: " + row + ", " + col);

        //Set the button to the player who went there!
        int result = tttGame.play(row, col);
        if(result == 1){
            buttons[row][col].setText("X");
        }else if(result == 2){
            buttons[row][col].setText("O");
        }else if(result == 0){
            Toast.makeText(this, "There has already been a play here!", Toast.LENGTH_LONG);
        }

        //Check to see if all of the places have been taken!
        if(tttGame.gameOver()){
            enableButtons(false);
            status.setBackgroundColor(Color.RED);
            //Set status text view with the result of game
            status.setText(tttGame.result());
        }


    }

    public void enableButtons(boolean enable){
        for(int i = 0; i < TicTacToe.SIDE; i++){
            for(int k = 0; k < TicTacToe.SIDE; k++) {
                buttons[i][k].setEnabled(enable);
            }
        }
    }


    private class ButtonHandler implements View.OnClickListener{
        @Override
        public void onClick(View view){
            Log.w("MainActivity", "view = " + view);

            for(int i = 0; i < TicTacToe.SIDE; i++){
                for(int k = 0; k < TicTacToe.SIDE; k++){
                    if(view == buttons[i][k]){
                        update(i, k);
                    }
                }
            }
        }
    }
}

