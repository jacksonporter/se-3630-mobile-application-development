package com.example.jacksonporter.tictactoev5;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Global variables
    private Button[][] buttons;
    private TicTacToe tttGame;
    private TextView status;
    private GridButtonAndTextView tttView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main)

        tttGame = new TicTacToe();

        //We are creating are GUI by coding it in dynamically.

        Point size = new Point(); //Get width of the screen
        getWindowManager().getDefaultDisplay().getSize(size);
        int w = size.x / TicTacToe.SIDE; //Calcuate the width of the buttons
        MainActivity.ButtonHandler bh = new MainActivity.ButtonHandler(); //Create a button handler
        tttView = new GridButtonAndTextView(this, w, 3, bh); //Create a GridButtonAndTextView layout
        tttView.setStatusText("Play!"); //Set the text in the inner text view
        setContentView(tttView); //Set the layout as the one we created above
    }


    public void showNewGameDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(tttGame.result());
        alert.setMessage("Do you want to play again?");

        PlayDialog playAgain = new PlayDialog();

        alert.setPositiveButton("YES", playAgain);
        alert.setNegativeButton("NO", playAgain);

        alert.show();
    }

    private class PlayDialog implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            if(i == -1){
                tttGame.resetGame();
                tttView.resetButtons();
                tttView.setStatusBackgroundColor(Color.GREEN);
                tttView.setStatusText("Play!");
            }else if(i == -2){
                MainActivity.this.finish();
            }
        }
    }

    private class ButtonHandler implements View.OnClickListener{
        @Override
        public void onClick(View view){
            Log.w("MainActivity", "view = " + view);

            for(int i = 0; i < TicTacToe.SIDE; i++){
                for(int k = 0; k < TicTacToe.SIDE; k++){

                    if(tttView.isButton((Button) view, i, k)) {
                        int play = tttGame.play(i, k);
                        if (play == 1) {
                            tttView.setButtonText(i, k, "X");
                        } else if (play == 2) {
                            tttView.setButtonText(i, k, "O");
                        } else if (play == 0) {
                            Toast.makeText(MainActivity.this, "There has already been a play here!", Toast.LENGTH_LONG);
                        }

                        if (tttGame.gameOver()) {
                            tttView.setStatusBackgroundColor(Color.RED);
                            tttView.enableButtons(false);
                            tttView.setStatusText(tttGame.result());
                            showNewGameDialog();
                        }
                    }
                }
            }
        }
    }
}

