package com.example.jacksonporter.tictactoev5;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

/**
 * Created by jackson on 2/18/18.
 */

public class GridButtonAndTextView extends GridLayout {
    private int side;
    private Button[][] buttons;
    private TextView status;

    public GridButtonAndTextView(Context context, int width, int newSide, View.OnClickListener listener){
        super(context);

        this.side = newSide;


        //SET UP STUFF

        this.setColumnCount(TicTacToe.SIDE);
        this.setRowCount(TicTacToe.SIDE + 1);

        //Create buttons and add them to the GridLayout
        //Also add a listener to complete an action for the buttons (display 'X')
        buttons = new Button[TicTacToe.SIDE][TicTacToe.SIDE];


        for(int i = 0; i < TicTacToe.SIDE; i++){
            for(int k = 0; k < TicTacToe.SIDE; k++){
                buttons[i][k] = new Button(context);
                buttons[i][k].setOnClickListener(listener);
                buttons[i][k].setTextSize((int) (width * .18));
                this.addView(buttons[i][k], width, width);
            }
        }


        //Add status text view
        GridLayout.Spec rowSpec = GridLayout.spec(3, 1);
        GridLayout.Spec columnSpec = GridLayout.spec(0, 3);
        GridLayout.LayoutParams lp = new GridLayout.LayoutParams(rowSpec, columnSpec);

        status = new TextView(context);
        status.setLayoutParams(lp);
        //Set properties of Status Text View
        status.setBackgroundColor(Color.GREEN);
        status.setWidth(TicTacToe.SIDE * width);
        status.setHeight(width);
        status.setGravity(Gravity.CENTER);

        status.setText("Play!");
        status.setTextSize((int) (width * .1));


        //Add status textview to the layout
        this.addView(status);
    }

    public void setStatusText(String text){
        status.setText(text);
    }

    public void setStatusBackgroundColor(int color){
        status.setBackgroundColor(color);
    }

    public void setButtonText(int row, int col, String text){
        buttons[row][col].setText(text);
    }

    public void enableButtons(boolean enable){
        for(int i = 0; i < TicTacToe.SIDE; i++){
            for(int k = 0; k < TicTacToe.SIDE; k++) {
                buttons[i][k].setEnabled(enable);
            }
        }
    }

    public void resetButtons(){

        enableButtons(true);

        for(int i = 0; i < TicTacToe.SIDE; i++){
            for(int k = 0; k < TicTacToe.SIDE; k++) {
                buttons[i][k].setText("");
            }
        }
    }

    public boolean isButton(Button b, int row, int col){
        if(buttons[row][col] == b){
            return true;
        }else{
            return false;
        }
    }
}
