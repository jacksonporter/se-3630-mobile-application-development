package com.example.jacksonporter.tictactoev1;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;

public class MainActivity extends AppCompatActivity {
    //Global variables
    private Button[][] buttons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main)

        //We are creating are GUI by coding it in dynamically.
        buildGUIbycode();
    }

    public void buildGUIbycode(){
        //Get width of the screen
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        //Calcuate the width of the buttons
        int w = size.x / TicTacToe.SIDE;

        //Create GridLayout as the layout manager
        GridLayout gridLayout = new GridLayout(this);
        gridLayout.setColumnCount(TicTacToe.SIDE);
        gridLayout.setRowCount(TicTacToe.SIDE);

        //Create buttons and add them to the GridLayout
        //Also add a listener to complete an action for the buttons (display 'X')
        buttons = new Button[TicTacToe.SIDE][TicTacToe.SIDE];
        ButtonHandler bh = new ButtonHandler();

        for(int i = 0; i < TicTacToe.SIDE; i++){
            for(int k = 0; k < TicTacToe.SIDE; k++){
                buttons[i][k] = new Button(this);
                buttons[i][k].setOnClickListener(bh);
                buttons[i][k].setTextSize((int) (w * .18));
                gridLayout.addView(buttons[i][k], w, w);
            }
        }




        setContentView(gridLayout);
    }

    public void update(int row, int col){
        Log.w("MainActivity", "Inside update: " + row + ", " + col);

        buttons[row][col].setText("X");
    }


    private class ButtonHandler implements View.OnClickListener{
        @Override
        public void onClick(View view){
            Log.w("MainActivity", "view = " + view);

            for(int i = 0; i < TicTacToe.SIDE; i++){
                for(int k = 0; k < TicTacToe.SIDE; k++){
                    if(view == buttons[i][k]){
                        update(i, k);
                    }
                }
            }
        }
    }
}

