package com.example.jacksonporter.tictactoev6;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {
    //Global variables
    private Button[][] buttons;
    private TicTacToe tttGame;
    private TextView status;
    private GridButtonAndTextView tttView;

    //Server Variables
    private InetAddress serverIP;
    private Integer portNumber;
    private Socket socket;
    private InputStream sIn;
    private DataInputStream socketDIS;
    private OutputStream sOut;
    private DataOutputStream socketDOS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main)

        tttGame = new TicTacToe();

        //We are creating are GUI by coding it in dynamically.

        Point size = new Point(); //Get width of the screen
        getWindowManager().getDefaultDisplay().getSize(size);
        int w = size.x / TicTacToe.SIDE; //Calcuate the width of the buttons
        MainActivity.ButtonHandler bh = new MainActivity.ButtonHandler(); //Create a button handler
        tttView = new GridButtonAndTextView(this, w, 3, bh); //Create a GridButtonAndTextView layout
        tttView.setStatusText("Play!"); //Set the text in the inner text view
        setContentView(tttView); //Set the layout as the one we created above

        new ConnectToServer().execute();
    }


    public void showNewGameDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(tttGame.result());
        alert.setMessage("Do you want to play again?");

        PlayDialog playAgain = new PlayDialog();

        alert.setPositiveButton("YES", playAgain);
        alert.setNegativeButton("NO", playAgain);

        alert.show();
    }

    private class PlayDialog implements DialogInterface.OnClickListener{

        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            if(i == -1){
                tttGame.resetGame();
                tttView.resetButtons();
                tttView.setStatusBackgroundColor(Color.GREEN);
                tttView.setStatusText("Play!");
            }else if(i == -2){
                MainActivity.this.finish();
            }
        }
    }

    private class ButtonHandler implements View.OnClickListener{
        @Override
        public void onClick(View view){
            Log.w("MainActivity", "view = " + view);

            for(int i = 0; i < TicTacToe.SIDE; i++){
                for(int k = 0; k < TicTacToe.SIDE; k++){

                    if(tttView.isButton((Button) view, i, k)) {
                        int play = tttGame.play(i, k);
                        if (play == 1) {
                            tttView.setButtonText(i, k, "X");
                        } else if (play == 2) {
                            tttView.setButtonText(i, k, "O");
                        } else if (play == 0) {
                            Toast.makeText(MainActivity.this, "There has already been a play here!", Toast.LENGTH_LONG);
                        }

                        if (tttGame.gameOver()) {
                            tttView.setStatusBackgroundColor(Color.RED);
                            tttView.enableButtons(false);
                            tttView.setStatusText(tttGame.result());
                            showNewGameDialog();
                        }
                    }
                }
            }
        }
    }

    public void postToastMessage(final String message, final int length){
        //Handler handler = new Handler(Looper.getMainLooper());

        /*handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getBaseContext(), message, length).show();
            }
        });*/

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getBaseContext(), message, length).show();
                //TextView answer = (TextView) findViewById(R.id.answertextview);
                //answer.setText("Answer: " + message);
            }
        });
    }

    private class ConnectToServer extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                //postToastMessage("New thread created.", Toast.LENGTH_LONG);
                postToastMessage("Trying to connect...", Toast.LENGTH_SHORT);
                String ipAddress = "34.218.52.6";
                portNumber = 5000;
                serverIP = InetAddress.getByName(ipAddress);

                socket = new Socket(serverIP, portNumber);
                postToastMessage("I connected to the Server!", Toast.LENGTH_SHORT);

                sIn = socket.getInputStream();
                socketDIS = new DataInputStream(sIn);
                sOut = socket.getOutputStream();
                socketDOS = new DataOutputStream(sOut);

                while(true){
                    String message = socketDIS.readLine();
                    postToastMessage(message, Toast.LENGTH_LONG);
                }


                /*//Clean up connection
                socketDOS.close();
                sOut.close();
                socketDIS.close();
                sIn.close();
                socket.close();
*/

            } catch (IOException e) {
                //printToastMessage("Couldn't connect to Server!!", Toast.LENGTH_SHORT);
                Thread.interrupted();
            }

            return null;
        }
    }
}

