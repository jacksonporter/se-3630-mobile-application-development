# TipCalculatorV4
## Providing basic functionality, this tip calculator allow you to enter a total for your bill as well as a tip percentage and give you your tip!
### This project was built using Android Studio.
#### It will give the total tip and total overall cost that you pay at the dinner table. All you need to do is type and it does the rest! Code provided by Kristal Ray of Snow College. This activity allowed me to further my knowledge of Event Listeners and XML. http://jacksonporter.jimdo.com
#### Download the APK here: https://github.com/jacksonporter/TipCalculator/raw/master/app/build/outputs/apk/debug/app-debug.apk
