package edu.snow.tipcalculatorv4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {
    private TipCalculator tipCalc;
    private NumberFormat money = NumberFormat.getCurrencyInstance();
    EditText billEditText, tipEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Call super class constructor
        super.onCreate(savedInstanceState);

        //Set the content view
        setContentView(R.layout.activity_main);

        //Create a TipCalculator instance
        tipCalc = new TipCalculator();

        //Create a TextChangeHandler instance.
        TextChangeHandler tch = new TextChangeHandler();

        //Set edit text variables by the id (they are XML items on a view)
        billEditText = (EditText) findViewById(R.id.amount_bill);
        tipEditText = (EditText) findViewById(R.id.amount_tip_percent);

        //Add a TextChangedListener to the above referenced EditTexts
        billEditText.addTextChangedListener(tch);
        tipEditText.addTextChangedListener(tch);
    }

    public void calculate( )
    {
        Log.w( "MainActivity", "v = ");
        String billString = billEditText.getText().toString();
        String tipString = tipEditText.getText().toString();

        TextView tipTextView = (TextView) findViewById(R.id.amount_tip);
        TextView totalTextView = (TextView) findViewById(R.id.amount_total);

        try
        {
            //Convert our input into number type variables so we can do calculations!
            Float billAmount = Float.parseFloat(billString);
            Integer tipPercent = Integer.parseInt(tipString);

            //Have the tipCalculator class do some math!
            tipCalc.setBill(billAmount);
            tipCalc.setTip(0.01f * tipPercent);

            //Retrieve the calculated values
            float tip = tipCalc.tipAmount();
            float total = tipCalc.totalAmount();

            //Update the view with the calculated values
            tipTextView.setText(money.format(tip));
            totalTextView.setText(money.format(total));


        }catch(NumberFormatException nfe)
        {
            //Eventually we will pop up an alert (view) here.
        }
    }





    public class TextChangeHandler implements TextWatcher {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {};

        public void onTextChanged(CharSequence s, int start, int count, int after) {};

        public void afterTextChanged(Editable e){
            calculate();
        }

    }
}
