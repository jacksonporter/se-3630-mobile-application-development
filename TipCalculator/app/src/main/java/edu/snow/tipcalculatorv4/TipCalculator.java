package edu.snow.tipcalculatorv4;

/**
 * Created by Jackson Porter on 1/23/18.
 * Code was given by Kristal Ray, professor at Snow College on 1/23/2018
 */

public class TipCalculator {

    private float tip;
    private float bill;

    public TipCalculator(float newTip, float newBill){
        setTip(newTip);
        setBill(newBill);
    }

    public TipCalculator()
    {

    }

    public float getTip() {
        return tip;
    }

    public float getBill(){
        return bill;
    }

    public void setTip(float newTip) {
        if(newTip > 0)
            tip = newTip;
    }

    public void setBill(float newBill) {
        if(newBill > 0)
            bill = newBill;
    }

    public float tipAmount(){
        return bill * tip;
    }

    public float totalAmount() {
        return bill + tipAmount();
    }
}
