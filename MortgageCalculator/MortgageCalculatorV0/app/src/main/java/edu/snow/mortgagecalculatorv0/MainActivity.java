package edu.snow.mortgagecalculatorv0;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void modifyData(View view){
        Intent dataIntent = new Intent(getApplicationContext(), ActivityData.class);
        startActivity(dataIntent);
    }
}
