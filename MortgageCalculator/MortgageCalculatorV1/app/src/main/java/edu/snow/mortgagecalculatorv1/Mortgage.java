package edu.snow.mortgagecalculatorv1;

import java.text.DecimalFormat;

/**
 * Created by jacks on 3/2/2018.
 */

public class Mortgage {
    public final DecimalFormat MONEY = new DecimalFormat("$#,##0.00");

    private float amount, rate;
    private int years;

    public Mortgage(){
        setAmount(100000.0f);
        setYears(30);
        setRate(0.035f);
    }


    public void setAmount(float amount) {
        if(amount >= 0){
            this.amount = amount;
        }
    }

    public void setYears(int years) {
        if(years >= 0){
            this.years = years;
        }

    }

    public void setRate(float rate) {
        if(rate >= 0){
            this.rate = rate;
        }
    }

    public float getAmount(){
        return this.amount;
    }

    public String getFormattedAmount(){
        return MONEY.format(this.amount);
    }

    public int getYears(){
        return this.years;
    }
}
