package edu.snow.mortgagecalculatorv3;

import java.text.DecimalFormat;

/**
 * Created by jacks on 3/2/2018.
 */

public class Mortgage {
    public final DecimalFormat MONEY = new DecimalFormat("$#,##0.00");

    private float amount, rate;
    private int years;

    public Mortgage(){
        setAmount(100000.0f);
        setYears(30);
        setRate(0.035f);
    }


    public void setAmount(float amount) {
        if(amount >= 0){
            this.amount = amount;
        }
    }

    public void setYears(int years) {
        if(years >= 0){
            this.years = years;
        }

    }

    public void setRate(float rate) {
        if(rate >= 0){
            this.rate = rate;
        }
    }

    public String getMonthlyPayment(){
        return MONEY.format(0.0f);
    }

    public String getTotalPayment(){
        return MONEY.format(0.0f);
    }

    public float getAmount(){
        return this.amount;
    }

    public String getFormattedAmount(){
        return MONEY.format(this.amount);
    }

    public int getYears(){
        return this.years;
    }

    public float getRate() {
        return rate;
    }

    public float monthlyPayment(){
        float mRate = rate / 12; //monthly interest rate
        double temp = Math.pow(1/(1+mRate), years * 12);
        return amount * mRate / (float) (1 - temp);
    }

    public String getFormattedMonthlyPayment(){
        return MONEY.format(monthlyPayment());
    }

    public float totalPayment(){
        return monthlyPayment() * years * 12;
    }

    public String getFormattedTotalPayment(){
        return MONEY.format(totalPayment());
    }
}
