package edu.snow.mortgagecalculatorv3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class ActivityData extends AppCompatActivity {
    public static final String DA = "DataActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
    }

    public void goBack(View view){
        Log.w(DA, "Inside ActivityData:goBack\n");
        updateMortgageObject();
        this.finish();
        overridePendingTransition(R.anim.fade_in_and_scale, 0);
    }

    protected void onStart(){
        super.onStart();
        Log.w(DA, "Inside ActivityData:onStart\n");
        updateView();
    }

    protected void onRestart() {
        super.onRestart();
        Log.w(DA, "Inside ActivityData:onRestart\n");
    }

    protected void onResume() {
        super.onResume();
        Log.w(DA, "Inside ActivityData:onResume\n");
    }

    protected void onPause() {
        super.onPause();
        Log.w(DA, "Inside ActivityData:onPause\n");
    }

    protected void onStop() {
        super.onStop();
        Log.w(DA, "Inside ActivityData:onsStop\n");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.w(DA, "Inside ActivityData:onDestroy\n");
    }

    public void updateView(){
        Mortgage mortgage = MainActivity.mortgage;

        if(mortgage.getYears() == 10){
            RadioButton rb10 = (RadioButton) findViewById(R.id.ten);
            rb10.setChecked(true);
        }else if(mortgage.getYears() == 15){
            RadioButton rb15 = (RadioButton) findViewById(R.id.fifteen);
            rb15.setChecked(true);
        }//else do nothing as 30 is the default on the view.

        EditText amountET = (EditText) findViewById(R.id.data_amount);
        amountET.setText("" + mortgage.getAmount());

        EditText rateET = (EditText) findViewById(R.id.data_rate);
        rateET.setText("" + mortgage.getRate());
    }

    public void updateMortgageObject(){
        Log.w(DA, "Inside ActivityData:updateMortgageObject\n");
        Mortgage mortgage = MainActivity.mortgage;

        //update the years in model
        RadioButton rb10 = (RadioButton) findViewById(R.id.ten);
        RadioButton rb15 = (RadioButton) findViewById(R.id.fifteen);
        int years = 30;

        if(rb10.isChecked()){
            years = 10;
        }else if(rb15.isChecked()){
            years = 15;
        }

        mortgage.setYears(years);

        //Update the amount in model
        EditText amountET = (EditText) findViewById(R.id.data_amount);
        String amountString = amountET.getText().toString();
        Log.w(DA, "Trying to set amount to: " + amountString);

        try{
            float amount = Float.parseFloat(amountString);
            mortgage.setAmount(amount);
        }catch(NumberFormatException nfe){
            Log.w(DA, "Amount was not formatted correctly. Setting back to default.");
            mortgage.setAmount(10000.0f);
        }



        //Update the interest rate in model
        EditText rateET = (EditText) findViewById(R.id.data_rate);
        String rateString = rateET.getText().toString();

        try{
            float rate = Float.parseFloat(rateString);
            mortgage.setRate(rate);
        }catch(NumberFormatException nfe){
            Log.w(DA, "Rate was not formatted correctly. Setting back to default.");
            mortgage.setRate(0.035f);
        }
    }
}
