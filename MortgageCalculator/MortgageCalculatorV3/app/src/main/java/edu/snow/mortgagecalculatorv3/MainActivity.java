package edu.snow.mortgagecalculatorv3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static final String MA = "MainActivity";
    public static Mortgage mortgage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mortgage = new Mortgage();

        setContentView(R.layout.activity_main);
    }

    public void modifyData(View view){
        Intent dataIntent = new Intent(getApplicationContext(), ActivityData.class);
        startActivity(dataIntent);
        overridePendingTransition(R.anim.slide_from_left, 0);
    }

    protected void onStart(){
        super.onStart();
        Log.w(MA, "Inside MainActivity:onStart\n");

        updateView();
    }

    protected void onRestart() {
        super.onRestart();
        Log.w(MA, "Inside MainActivity:onRestart\n");
    }

    protected void onResume() {
        super.onResume();
        Log.w(MA, "Inside MainActivity:onResume\n");
    }

    protected void onPause() {
        super.onPause();
        Log.w(MA, "Inside MainActivity:onPause\n");
    }

    protected void onStop() {
        super.onStop();
        Log.w(MA, "Inside MainActivity:onsStop\n");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.w(MA, "Inside MainActivity:onDestroy\n");
    }

    public void updateView(){

        //Create objects to reference on the view
        TextView amountTV = (TextView) findViewById(R.id.amount);
        amountTV.setText(mortgage.getFormattedAmount());

        TextView yearsTV = (TextView) findViewById(R.id.years);
        yearsTV.setText("" + mortgage.getYears());

        TextView rateTV = (TextView) findViewById(R.id.rate);
        rateTV.setText("" + (100 * mortgage.getRate()) + "%");

        TextView monthpaymentTV = (TextView) findViewById(R.id.monthpayment);
        monthpaymentTV.setText(mortgage.getFormattedMonthlyPayment());

        TextView totalpaymentTV = (TextView) findViewById(R.id.totalpayment);
        totalpaymentTV.setText(mortgage.getFormattedTotalPayment());
    }
}
