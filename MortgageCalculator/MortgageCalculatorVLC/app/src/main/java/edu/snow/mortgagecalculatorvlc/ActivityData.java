package edu.snow.mortgagecalculatorvlc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class ActivityData extends AppCompatActivity {
    public static final String DA = "DataActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
    }

    public void goBack(View view){
        this.finish();
    }

    protected void onStart(){
        super.onStart();
        Log.w(DA, "Inside ActivityData:onStart\n");
    }

    protected void onRestart() {
        super.onRestart();
        Log.w(DA, "Inside ActivityData:onRestart\n");
    }

    protected void onResume() {
        super.onResume();
        Log.w(DA, "Inside ActivityData:onResume\n");
    }

    protected void onPause() {
        super.onPause();
        Log.w(DA, "Inside ActivityData:onPause\n");
    }

    protected void onStop() {
        super.onStop();
        Log.w(DA, "Inside ActivityData:onsStop\n");
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.w(DA, "Inside ActivityData:onDestroy\n");
    }
}
