package edu.snow.trafficlightv0;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private LinearTrafficLightLayout myLayout;
    private TrafficLight trafficLight;
    private ButtonHandler bh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        trafficLight = new TrafficLight(TrafficLight.REDLIGHT);


        bh = new ButtonHandler();
        myLayout = new LinearTrafficLightLayout(this, bh, trafficLight.getCurrentColor());
        setContentView(myLayout);

        //setContentView(R.layout.activity_main);
    }


    private class ButtonHandler implements View.OnClickListener{
        @Override
        public void onClick(View view){
            Log.w("MainActivity", "view = " + view);

            myLayout.setLightColor(trafficLight.changeLight());
        }
    }
}
