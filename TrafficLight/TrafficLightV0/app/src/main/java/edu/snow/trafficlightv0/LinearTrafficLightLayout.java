package edu.snow.trafficlightv0;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by jackson on 2/25/18.
 */

public class LinearTrafficLightLayout extends LinearLayout {

    private Button changeLightButton;
    private TextView lightColor;
    private int currentColor;
    private View.OnClickListener listener;


    public LinearTrafficLightLayout(Context context, View.OnClickListener listener, int color) {
        super(context);

        //Initialize global variables
        this.listener = listener;
        changeLightButton = new Button(context);
        lightColor = new TextView(context);

        //Set variable attributes
        changeLightButton.setText("Change Light");
        changeLightButton.setOnClickListener(listener);
        //lightColor.setText("TEXTVIEW");
        //lightColor.setGravity(Gravity.CENTER);
        setLightColor(color);


        //Set layout to have horizontal views
        this.setOrientation(LinearLayout.VERTICAL);


        this.addView(lightColor);
        this.addView(changeLightButton);
    }

    public void setLightColor(int color){

        if(color == TrafficLight.REDLIGHT){
            lightColor.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.redlight, 0, 0);
            currentColor = TrafficLight.REDLIGHT;
        }
        else if(color == TrafficLight.YELLOWLIGHT){
            lightColor.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.yellowlight, 0, 0);
            currentColor = TrafficLight.YELLOWLIGHT;
        }
        else if(color == TrafficLight.GREENLIGHT){
            lightColor.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.greenlight, 0, 0);
            currentColor = TrafficLight.GREENLIGHT;
        }
    }

    public int currentColor(){
        return currentColor;
    }
}
