package edu.snow.trafficlightv0;

/**
 * Created by jackson on 2/25/18.
 */

public class TrafficLight {
    public static int REDLIGHT = 1;
    public static int YELLOWLIGHT = 2;
    public static int GREENLIGHT = 3;

    private int color;

    public TrafficLight(int color)
    {
        this.color = color;
    }

    public int changeLight(){
        if(color == this.REDLIGHT){
            this.color = this.GREENLIGHT;
        }
        else if(color == this.GREENLIGHT){
            this.color = this.YELLOWLIGHT;
        }
        else if(color == this.YELLOWLIGHT){
            this.color = this.REDLIGHT;
        }
        else{
            return -1;
        }

        return this.color;
    }

    public int getCurrentColor(){
        return this.color;
    }
}
