package com.example.jacksonporter.testapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class CreateUserActivity extends AppCompatActivity {
    boolean male = true;
    String gender = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
    }

    public void onClick(View view)
    {
        EditText input = (EditText) findViewById(R.id.username);
        String string = input.getText().toString();
        if(male)
        {
            gender = "Male";
        }
        else
        {
            gender = "Female";
        }
        Toast.makeText(this, "User " + string + " created. " + gender + " selected.", Toast.LENGTH_LONG).show();

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.gender);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.male:
                        male = true;
                        break;
                    case R.id.female:
                        male = false;
                        break;
                }
            }
        });


    }
}
